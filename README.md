# Apuntes LFS201<a id="orgheadline1"></a>

Este repositorio contiene tanto mis apuntes personales cómo el curso completo para la [certificación](https://training.linuxfoundation.org/linux-courses/system-administration-training/essentials-of-system-administration) "Essentials of System Administration". El índice del repositorio es el siguiente:

-   `apuntes.*`: Esto son los apuntes que he hecho yo. Están en cuatro formatos distintos, en PDF, en HTML en TEX y en org-mode.

-   `curso_completo.html`: El curso completo.

-   `imgs`: Imagenes que aparecen en el curso completo.

-   `labs`: Los ejercicios que aparecen en el curso completo y algunos otros recursos.

-   `styles`: CSS tanto del curso cómo de los apuntes.

Cosas a tener en cuenta en general:

-   Es posible y lógico pensar que en el transcurso del tiempo el contenido de este curso cambie, así que recomiendo encarecidamente comprobar si es así, ya que aunque seguramente la mayoria de los recursos de este repositorio seguirán sirviendo, es posible que hayan más cosas. La última fecha en la que yo he comprobado que estos no hancambiado es en 13/08/2016.

-   En todo el curso obvian bastante a GNU, por lo que cuando sale **GNU/Linux** en verdad ponia **Linux** y lo he cambiado yo, por eso de que un kernel no es un sistema operativo, por mucho que insistan desde la Linux Fundation.

-   A título de curiosidad, mi experiencia con este curso me ha mostrado que a la Linux Fundation no le importa absolutamente nada la libertad, no sólo por lo comentado en el anterior párrafo, sinó por que para hacer el exámen <span class="underline">obligan</span> a usar un navegador en concreto, **chromium**, y a instalar una extensión de código privativo (hasta dónde sé) que se llama Innovative Exams ScreenSharing.

-   Si no se quiere clonar el repositorio y sólo se quiere mirar los HTML o los PDF, se puede hacer:
    -   [Apuntes HTML](https://daemons.it/LFS201/apuntes.html)

    -   [Apuntes PDF](https://daemons.it/LFS201/apuntes.pdf)

    -   [Curso HTML](https://daemons.it/LFS201/curso-completo.html)

Cosas a tener en cuenta respecto los apuntes:

-   En general, en los apuntes los he tomado yo, pero en algún caso he copiado literalmente lo que ponia en el curso. En todos (o casi todos) los casos en los que he hecho eso, he puesto una nota diciendo que es una copia literal, por lo del copyright y eso.

-   Cuando empecé el curso se podian copiar las diapositivas del curso, pero desde aproximadamente el Febrero de 2016 esto no es posible, cuando clicas sólo cambia de diapositiva. Imagino que para que no se copie. Ya veis lo que les ha servido.

-   Cuando sale **Apunte:** es un añadido que he puesto, para dar una opinión personal o lo que sea.

-   El orden del curso parece que sigue un orden desde que se enciende el ordenador en adelante. Por esto, hay algunos capítulos que se ven un poco tontos en el sitio que están, ya que a las alturas a las que llegas a el, cómo no sepas eso, no has podido seguir lo anterior.

-   En todo el texto uso tanto pronombres femeninos cómo masculinos para referirme a un sujeto genérico.

-   Hay algunos subcapítulos que pueden parecer demasiado cortos. Esto es cierto, los he resumido por que en el curso hay veces que sólo repiten la misma información con distintas palabras.
